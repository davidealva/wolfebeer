###Landing page for Wolfe Beer, the worlds most amazing tasting Rootbeer ever…###

This app is a Ruby on Rails app with an AngularJS frontend. Rails is being used to serve up a simple JSON api and can be seen at:

**localhost:3000/buyers.json**

To set up and run app locally:

**git clone git@bitbucket.org:davidealva/wolfebeer.git**

Then run:

**bundle install**

**rake db:migrate**

Sample data for the feed is created by Faker gem, please run:

**rake db:populate**

and then:

**rails server**

And now the app is ready to run at:

**localhost:3000**